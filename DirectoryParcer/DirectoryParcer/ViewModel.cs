﻿using System;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using DirectoryParcer.Model;
using System.Threading;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Threading;
//using System.Windows.Forms;

namespace DirectoryParcer
{
    struct Data
    {
        public string path;
        public Dir current;

    }

    public class ViewModelBase : INotifyPropertyChanged
    {
       //Fields
        private string _sourcepath;
        private string _destinationpath;
        private Dir _result;
        private TreeView _trv;
        private Thread _info_collector;
        private Thread _xml_writer_thread;
        private Thread _treeview_writer_thread;

        //Properties
        public string SourcePath
        {
            get
            {
                return _sourcepath;
            }

            set
            {
                _sourcepath = value;
                OnPropertyChanged("SourcePath");
            }
        }
        public string DestinationPath
        {
            get
            {
                return _destinationpath;
            }
            set
            {
                _destinationpath = value;
                OnPropertyChanged("DestinationPath");
            }
        }
        public Dir Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;
                OnPropertyChanged("Result");
            }
        }
        public TreeView Trv
        {
            get { return _trv; }
            set
            {
                _trv = value;
                OnPropertyChanged("Trv");
            }
        }
        public ICommand Start_Command { get; set; }
        public ICommand Exit_Command { get; set; }
        public ICommand Choose_Command { get; set; }
        public ICommand Save_Command { get; set; }

        //Events
        public event PropertyChangedEventHandler PropertyChanged;

        // Constructors
        public ViewModelBase()
        {
            Start_Command = new RelayCommand(arg => StartMethod());
            Exit_Command = new RelayCommand(arg => ExitMethod());
            Choose_Command = new RelayCommand(arg => ChooseMethod());
            Save_Command = new RelayCommand(arg => SaveMethod());
            //  DirCollection = new List<IItem>();
            _info_collector = new Thread(Parcer);
            _xml_writer_thread = new Thread(XMLWriter);
            _treeview_writer_thread = new Thread(TreeViewWriter);
        }

        // INotifyPropertyChanged Implementation
        public virtual void OnPropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }


        // Button Commands handlers
        public void StartMethod()
        {
            // Этот метод запускается при нажатии кнопки "Старт" на форме
            Data dt = new Data { path = SourcePath, current = new Dir() };
            //_info_collector.Start(SourcePath);
            try
            {
            _info_collector.Start(dt);
            _info_collector.Join();
            _xml_writer_thread.Start();
            Trv = new TreeView();
            _treeview_writer_thread.Start();

            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("повторный запуск программы невозможеню. Необходимо выйти из программы и зайти повторно");
            }
            
           
         //   System.Windows.Forms.MessageBox.Show("Start Method   "+ Thread.CurrentThread.ManagedThreadId.ToString());
        }
        public void ExitMethod()
        {
            // Этот метод запускается при нажатии кнопки "Стоп" на форме. Выходит из программы так как перезапуск потока невозможен.
            StopThreads();
            Environment.Exit(0);
        }
        public void ChooseMethod()
        {
            System.Windows.Forms.FolderBrowserDialog _openDialog = new System.Windows.Forms.FolderBrowserDialog();

            if (_openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SourcePath = _openDialog.SelectedPath;
            }
        }
        public void SaveMethod()
        {
            try
            {
                System.Windows.Forms.SaveFileDialog _openDialog = new System.Windows.Forms.SaveFileDialog();
                _openDialog.InitialDirectory = (System.IO.Path.GetFullPath("C:\\"));
                _openDialog.Filter = "XML Files |*.xml";

                if (_openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    DestinationPath = _openDialog.FileName;
                }
            }
            catch(FileNotFoundException f)
            {
                //System.Windows.Forms.MessageBox.Show(f.Message);
                //Исключение вылетает всегда при задании существующего файла для повторной записи
                // Предполагаю, что дело в использовании стандартного диалогового окна WIndows Forms
                //Поскольку открытие файла происходит по маске в другом методе, а окно дает
                // павильную маску, то дополнительная обработка этого исключения не требуется
                //System.Windows.Forms.MessageBox.Show(f.Message);
                //return;
            }
            
        }

        // Folder Parcer
       

        // XML Writer
        private void XMLWriter(object Path)
        {
            string outputfile = "Default.xml";
            //System.Windows.Forms.MessageBox.Show("XML Writer Invoke  " + Thread.CurrentThread.ManagedThreadId.ToString());  // debugging block. Shows TreadID for Multithreading control
            if (DestinationPath != null)
            {
                string[] savePath = DestinationPath.Split('\\');
                string fileName = savePath[savePath.Length - 1];
                string pattern = @"\w*.xml";
                if (!Regex.IsMatch(fileName, pattern))
                {
                    System.Windows.Forms.MessageBox.Show("Неправильный формат файла. Проверьте имя и расширение файла для сохраниения данных. Расширение файла должно быть .xml");
                }
                else
                {
                    outputfile = DestinationPath;
                }
            }


            var tw = new XmlTextWriter(outputfile, Encoding.Default);

            tw.Formatting = Formatting.Indented;
            tw.Indentation = 8;
            tw.WriteStartDocument();
            SearcherXML(SourcePath, tw, Result);
            tw.WriteEndDocument();//12
            tw.Close();
            tw.Dispose();
            System.Windows.Forms.MessageBox.Show("Запись в ХML закончена");
        }
        private void SearcherXML(string Path, XmlTextWriter tw, Dir source)
        {
            // запись информации об объекте типа DIR
            tw.WriteStartElement("Folder");  // Открывающий тег объекта DIR
            CommonInfoXMLWriter(tw, source as IItem);
            tw.WriteStartElement("Inner_Elements");  // открывающий тэг объекта Сабс
            foreach (var item in source.Subs)
            {
                if (item is Dir)
                {
                    string path2 = Path + '\\' + item.Name;
                    SearcherXML(path2, tw, item as Dir);
                }
                if (item is Files)
                {
                    XMLFileWriter(tw, item as Files);
                }

            }
            tw.WriteEndElement();// закрывающий тэг объекта Сабс
            tw.WriteEndElement();// Закрывающий тег объекта DIR

        }
        private void XMLFileWriter(XmlTextWriter tw, Files source)
        {
            tw.WriteStartElement("File");
            CommonInfoXMLWriter(tw, source as IItem);
            tw.WriteStartElement("Extention");
            tw.WriteString(source.Extention);
            tw.WriteEndElement();
            tw.WriteEndElement();

        }
        private void CommonInfoXMLWriter(XmlTextWriter tw, IItem source)
        {
            tw.WriteStartElement("Name");
            tw.WriteString(source.Name);
            tw.WriteEndElement();
            tw.WriteStartElement("Creation_Date");
            tw.WriteString(source.CreationDate);
            tw.WriteEndElement();
            tw.WriteStartElement("Modified_Date");
            tw.WriteString(source.ModifiedDate);
            tw.WriteEndElement();
            tw.WriteStartElement("Last_Using");
            tw.WriteString(source.LastUsingDate);
            tw.WriteEndElement();
            tw.WriteStartElement("Attributes");
            tw.WriteString(source.Attribut);       //TODO: здесь может понадобиться разворачивание атрибутов в массив и соответственно, разворачивание тэга
            tw.WriteEndElement();
            tw.WriteStartElement("Size");          //TODO: Сделать метод подсчета размера директории
            tw.WriteString(source.Size);
            tw.WriteEndElement();
            tw.WriteStartElement("Owner");         
            tw.WriteString(source.Owner);
            tw.WriteEndElement();
            tw.WriteStartElement("Current_Permissions");   
            tw.WriteString(source.CurrentPermissions);
            tw.WriteEndElement();
        }

        // TreeView Writer
        private void TreeViewWriter()
        {

           // System.Windows.Forms.MessageBox.Show("Tree View Writer Invoke  " + Thread.CurrentThread.ManagedThreadId.ToString());  // debugging block. Shows TreadID for Multithreading control
            Trv.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (Action)delegate()
                {
                    TreeViewItem level0 = new TreeViewItem();
                    level0.Header = SourcePath;
                    level0.Tag = Result;
                    level0.Expanded += Level_Expanded;
                    level0.Items.Add("*");
                    Trv.Items.Add(level0);
                });


        }
        void Level_Expanded(object sender, System.Windows.RoutedEventArgs e)
        {
            Trv.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
               (Action)delegate()
               {
                   TreeViewItem item = (TreeViewItem)e.OriginalSource;
                   item.Items.Clear();
                   foreach (var prod in (item.Tag as Dir).Subs)
                   {
                       TreeViewItem newItem = new TreeViewItem();
                       newItem.Tag = prod;
                       newItem.Header = prod.Name;
                       if (prod is Dir)
                       {
                           TreeViewItem levelx = new TreeViewItem();
                           levelx.Header = prod.Name;
                           levelx.Tag = Result;
                           levelx.Expanded += Level_Expanded;
                           levelx.Items.Add("*");
                           newItem.Items.Add(levelx);
                       }
                       item.Items.Add(newItem);
                   }
               });
        }

         //Auxillary Methods
        private void StopThreads()
        {
            if (_info_collector.IsAlive) _info_collector.Interrupt();
            if (_xml_writer_thread.IsAlive) _xml_writer_thread.Interrupt();
            if (_treeview_writer_thread.IsAlive) _treeview_writer_thread.Interrupt();
        }



    }
}

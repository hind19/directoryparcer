﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryParcer.Model
{
    //Интерфейся, являющийся базовым для классов Dir ,  Fіles
    public interface IItem
    {
        string Name {get;set;}
        string CreationDate { get; set; }
        string ModifiedDate { get; set; }
        string LastUsingDate { get; set; }
        string Attribut { get; set; }
        string Size { get; set; }
        string Owner { get; set; }
        string CurrentPermissions { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.ObjectModel;
using System.Security.Principal;
using System.Security.AccessControl;

namespace DirectoryParcer.Model
{
    public class Dir : IItem
    {
        //Класс, описывающий папку (в т.ч содержит коллекцию дочерних элементов)
        //Properties
        public string Name { get; set; }
        public string CreationDate { get; set; }
        public string ModifiedDate { get; set; }
        public string LastUsingDate { get; set; }
        public string Attribut { get; set; }
        public string Size { get; set; }
        public string Owner { get; set; }
        public string CurrentPermissions { get; set; }
        public List<IItem> Subs { get; set; }

        //Ctors
        public Dir()
        {
            Subs = new List<IItem>();
        }
        public Dir(DirectoryInfo inf)
        {
            Subs = new List<IItem>();
            this.Name = inf.Name;
            this.CreationDate = inf.CreationTime.ToString();
            this.ModifiedDate = inf.LastWriteTime.ToString();
            this.LastUsingDate = inf.LastAccessTime.ToString();
            this.Attribut = inf.Attributes.ToString();
            //this.Size = GetDirectorySize();
            this.Owner = GetDirectoryOwner(inf);
            this.CurrentPermissions = GetCurrentPermissions(inf);
        }

        private string GetDirectoryOwner(DirectoryInfo inf)
        {
            //Получает собственника папки в строковом виде
            try
            {
                var fs = inf.GetAccessControl();
                var sid = fs.GetOwner(typeof(SecurityIdentifier));
                return sid.Translate(typeof(NTAccount)).ToString();
            }
            catch
            {
                return "Can't define Owner";

            }
        }

        private string GetCurrentPermissions(DirectoryInfo inf)
        {
            #region //Образец
            //DirectorySecurity acl = di.GetAccessControl(AccessControlSections.All);
            //AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(NTAccount));

            ////Go through the rules returned from the DirectorySecurity
            //foreach (AuthorizationRule rule in rules)
            //{
            //    //If we find one that matches the identity we are looking for
            //    if (rule.IdentityReference.Value.Equals(NtAccountName, StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        //Cast to a FileSystemAccessRule to check for access rights
            //        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.WriteData) > 0)
            //        {
            //            Console.WriteLine(string.Format("{0} has write access to {1}", NtAccountName, path));
            //        }
            //        else
            //        {
            //            Console.WriteLine(string.Format("{0} does not have write access to {1}", NtAccountName, path));
            //        }
            //    }
            //}
            #endregion
            // условно рабочий блок
            var fs = inf.GetAccessControl(AccessControlSections.Access);
            var perm = fs.GetAccessRules(true, true, typeof(SecurityIdentifier));

            var NtAccountName = WindowsIdentity.GetCurrent().Groups;  //M1/USER

            StringBuilder sbRules = new StringBuilder();
            for (int i = 0; i < NtAccountName.Count; i++)
            {
                foreach (FileSystemAccessRule rule in perm)
                {
                    if (rule.IdentityReference.Value == NtAccountName[i].Value)
                    {
                        //Cast to a FileSystemAccessRule to check for access rights
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.FullControl) > 0)
                        {
                            sbRules.Append("FullControl ");
                            return sbRules.ToString();
                        }

                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.WriteData) > 0)
                        {
                            sbRules.Append("WritrData" + " ");
                        }
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.CreateDirectories) > 0)
                        {
                            sbRules.Append("CtrateDirectories" + " ");
                        }
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.CreateFiles) > 0)
                        {
                            sbRules.Append("CreateFiles" + " ");
                        }
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.ExecuteFile) > 0)
                        {
                            sbRules.Append("ExecuteFiles" + " ");
                        }
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.CreateFiles) > 0)
                        {
                            sbRules.Append("CreateFiles" + " ");
                        }
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.Delete) > 0)
                        {
                            sbRules.Append("Delete" + " ");
                        }
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.Modify) > 0)
                        {
                            sbRules.Append("Modify" + " ");
                        }
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.Read) > 0)
                        {
                            sbRules.Append("Read" + " ");
                        }
                        if ((((FileSystemAccessRule)rule).FileSystemRights & FileSystemRights.Write) > 0)
                        {
                            sbRules.Append("Write" + " ");
                        }

                    }

                }

            }

#region    //Экспериментальный блок
           //AuthorizationRuleCollection rules = fs.GetAccessRules(true, true, typeof(NTAccount));

            //foreach (AuthorizationRule rule in rules)
            //{
            //    FileSystemAccessRule accessRule = rule as FileSystemAccessRule;
            //    sbRules.Append(", "+rule.IdentityReference.ToString());

            //}

            // Экспериментальый блок
            //  List<string> RulesCol0 = new List<string>();
            //  List<List<string>> RulesCol = new List<List<string>>();

            ////  System.Windows.Forms.MessageBox.Show(perm.Count.ToString());
            //  foreach (FileSystemAccessRule ace in perm)
            //  {
            //      RulesCol0.Add(ace.FileSystemRights.ToString());
            //      //RulesCol.Add(ace.FileSystemRights.ToString().Split(',').ToList());
            //      //  System.Windows.Forms.MessageBox.Show(ace.FileSystemRights.ToString());

            //      //Образец
            //     // //FileSystemRights
            //     //// Console.WriteLine("{0}Account: {1}",  ace.IdentityReference.Value);
            //     //// Console.WriteLine("{0}Type: {1}",  ace.AccessControlType);
            //     // Console.WriteLine("{0}Rights: {1}", ace.FileSystemRights);
            //     //// Console.WriteLine("{0}Inherited: {1}", ace.IsInherited);
            //     // Console.WriteLine();

            //}

#endregion
            return sbRules.ToString();
        }


        //private string GetDirectorySize()
        //{
        //    return string.Empty;
        //}



    }
}

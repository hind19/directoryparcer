﻿using DirectoryParcer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryParcer.Services
{
    public class ParcerService : IParcerService
    {
        public void Parcer(object data)
        {
            /*
             Этот метод отвечает за сбо информации об имеющихся директориях. 
             * На выходе получаем 
             *  уже готовые  к записи в XML файл и Tree View  данные
             *  в виде коллекции - поля класса.
             *  Пока он работает поледостаельно перед остальными.
             *  
             */

            try
            {
                //    System.Windows.Forms.MessageBox.Show("ParcerInvoke "+Thread.CurrentThread.ManagedThreadId.ToString());  // debugging block. Shows TreadID for Multithreading control
                Data d = (Data)data;
                string path_inner = d.path;
                if (!path_inner.EndsWith("\\"))
                    path_inner += "\\";

                DirectoryInfo dirs = new DirectoryInfo(path_inner);
                if (!dirs.Exists)
                {
                    throw new DirectoryNotFoundException();
                }

                Dir tmp = new Dir(dirs);
                if (path_inner == SourcePath || path_inner == (SourcePath + '\\'))
                    Result = tmp;
                else 
                    tmp = d.current;
                
                DirectoryInfo[] dir_sub = dirs.GetDirectories();
                
                FileInfo[] di_files = dirs.GetFiles();

                if (dir_sub.Length > 0)
                {

                    foreach (var item in dir_sub)
                    {
                        Dir tmp2 = new Dir(item);
                        tmp.Subs.Add(tmp2);
                        Data recurse_data = new Data { path = path_inner + item.Name, current = tmp2 };
                        this.Parcer(recurse_data);
                    }
                }
                if (di_files.Length > 0)
                {
                    foreach (var item in di_files)
                    {
                        tmp.Subs.Add(new Files(item));
                    }
                }
            }
            catch (Exception e)
            {
                throw e;

            }




        }
    }
}

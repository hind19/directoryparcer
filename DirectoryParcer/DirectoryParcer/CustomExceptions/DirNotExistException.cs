﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryParcer
{
    public class DirNotExistException : Exception
    {
        public override string Message => "No Directory has been found using this path";
        
    }
}
